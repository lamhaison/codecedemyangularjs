from django.db import models

# Create your models here.


class Books(models.Model):
    name = models.TextField(max_length=100)
    cover = models.TextField(max_length=100)
    price = models.IntegerField()
    pubdate = models.DateField()
    likes = models.IntegerField()
    dislikes = models.IntegerField()

    class Meta:
        db_table = 'books'