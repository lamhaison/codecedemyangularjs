__author__ = 'lhs'
from django.conf.urls import url, include, patterns
from bookstore import views
from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = patterns('',
    url(r'^home/$', views.home_page, name='home-page'),
    #url(r'^new/$', views.CreateContactView.as_view(), name='contacts-new'),
    #url(r'^edit/(?P<pk>\d+)/$', views.UpdateContactView.as_view(), name='contacts-edit'),



)

urlpatterns += [
    url(r'^books/$', views.BookList.as_view()),
    url(r'^books/(?P<pk>[0-9]+)/$', views.BookDetail.as_view())
]




