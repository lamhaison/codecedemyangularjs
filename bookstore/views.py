from django.shortcuts import render
from rest_framework import mixins
from rest_framework import generics
from django.contrib.auth.decorators import login_required
from bookstore.serializers import BookSerializer
from bookstore.models import Books


# Create your views here.


#@login_required
def home_page(request):
    return render(request, 'index.html')


class BookList(generics.ListCreateAPIView):
    # queryset = Books.objects.all()
    serializer_class = BookSerializer

    def get_queryset(self):
        queryset = Books.objects.all()
        name = self.request.query_params.get('name', None)
        print 'name: %s' % name
        if name is not None:
            return queryset.filter(name__icontains=name)

        return queryset



class BookDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Books.objects.all()
    serializer_class = BookSerializer

