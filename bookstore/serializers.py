from django.db import models
from django.contrib.auth.models import User, Group
from rest_framework import serializers
from bookstore.models import Books


class BookSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Books
        fields = ('id', 'name', 'price', 'pubdate', 'likes', 'dislikes', 'cover')



