/**
 * Created by root on 10/28/15.
 */
var app = angular.module("myApp", ['ngCookies']).config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('{$');
    $interpolateProvider.endSymbol('$}');
});