/**
 * Created by root on 10/28/15.
 */


(function () {

    var restapi = function ($http, $cookies) {
        $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
        console.log($cookies['csrftoken']);
        var getUser = function (user_id, token) {
            urlUser = '/api/users/' + user_id + '?format=json';
            //$log.debug('url: ' + urlUser);
            //$log.debug('token: ' + token);
            var req = {
                method: 'GET',
                url: urlUser,
                headers: {
                    'Authorization': 'Token ' + token
                }
            };
            return $http(req)
                .then(function (response) {
                    return response.data;
                });
        };

        var plusOne = function (book) {
            console.log(book);
            $http.defaults.headers.put['X-CSRFToken'] = $cookies['csrftoken'];
            console.log('csrftoken:' + $cookies['csrftoken']);
            url_like = '/practices/books/' + book.id + '/';
            console.log('url: ' + url_like);
            var req = {
                method: 'PUT',
                url: url_like,
                data: book
            };

            return $http(req)
                .then(function (response) {
                    return response.data;

                });

        };

        var getToken = function (username, password) {
            username = 'admin';
            password = 'admin';
            //$log.debug('username: ' + username);
            //$log.debug('password: ' + password);

            urlToken = '/api-token-auth/';
            //$log.debug('urlToken: ' + urlToken);
            var req = {
                method: 'POST',
                url: urlToken,
                data: {'username': username, 'password': password}
            };

            data = {'username': username, 'password': password};

            //return $http.get(urlToken)
            //    .then(function (response) {
            //        return response.data;
            //    });

            return $http(req)
                .then(function (response) {
                    return response.data;

                });

        };


        var getBooks = function (token) {
            //console.log($cookies['csrftoken']);
            //$http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
            var urlBooks = '/practices/books/?format=json';
            var req = {
                method: 'GET',
                url: urlBooks
                //headers: {
                //    'Authorization': 'Token ' + token
                //}
            };

            return $http(req)
                .then(function (response) {
                    return response.data;
                });

        };

        var getDetailBook = function (token, book_name) {
            //$http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
            //console.log($cookies['csrftoken']);
            var urlBook = '/practices/books/?format=json&name=' + book_name;

            console.log(urlBook);
            var req = {
                method: 'GET',
                url: urlBook,
                headers: {
                    'Authorization': 'Token ' + token
                }
            };

            return $http(req)
                .then(function (response) {
                    return response.data;
                });
        };


        //var getRepos = function (user) {
        //    return $http.get(user.repos_url)
        //        .then(function (response) {
        //            return response.data;
        //        });
        //};


        return {
            getUser: getUser,
            getToken: getToken,
            getBooks: getBooks,
            getDetailBook: getDetailBook,
            plusOne: plusOne
        };
    };


    var app = angular.module('myApp');
    app.factory("restapi", restapi);
}());

