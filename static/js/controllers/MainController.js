/**
 * Created by root on 10/28/15.
 */

(function () {
    var MainController = function ($scope, restapi) {


        //console.log($cookies['csrftoken']);
        $scope.title = 'This Month\'s Bestsellers';
        $scope.promo = 'The most popular books this month.';
        //$scope.token = 'b4148eadf0e7f73cc5f136c9e6616c474ef7db99';

        var like_success = function(data) {
            console.log('like not success');
        };

        var like_error = function(data) {
            console.log('like is not success');
        };

        $scope.plusOne = function (index) {
            $scope.products[index].likes += 1;
            restapi.plusOne($scope.products[index]).then(like_success,like_error);
        };

        $scope.minusOne = function (index) {
            $scope.products[index].dislikes += 1;
            restapi.plusOne($scope.products[index]).then(like_success,like_error);
        };

        var onComplete = function (data) {
            $scope.products = data.results;
        };

        var onError = function (data) {
            $scope.products = null;
            $scope.error = 'can not get data';
        };

        $scope.getBooks = function () {

            restapi.getBooks($scope.token).then(onComplete, onError);
        };


        var onDetailBook = function (data) {
            console.log(data);
            $scope.products = data.results;
            console.log($scope.products);
        };

        $scope.searchBook = function(book_name) {

            if (book_name != '') {
                restapi.getDetailBook($scope.token, book_name).then(onDetailBook, onError);

            }else{
                 $scope.getBooks();
            }

        };

        var getBooks = function () {
            $scope.getBooks();
        };

        getBooks();

    };

    var app = angular.module('myApp');
    app.controller('MainController', MainController);
}());